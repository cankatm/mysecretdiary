import React, { Component } from 'react';
import { 
    View,
    Text,
} from 'react-native';

import styles from './styles';
import * as defaults from '../../helpers/DefaultValues';

class CenteredArea extends Component {
    static defaultProps = {
        marginTop: 0,
    }

    render() {
        const { children, marginTop } = this.props;
        return (
            <View style={[styles.centeredAreaContainerStyle, { marginTop }]} >
                {children}
            </View>
        );
    }
}

export default CenteredArea;