import {
    CHANGE_DAY
} from '../helpers/ActionNames.js';

export const changeDayAction = (day) => {
    return {
        type: CHANGE_DAY,
        payload: day
    };
}