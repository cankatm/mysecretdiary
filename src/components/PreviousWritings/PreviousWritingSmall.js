import React, { Component } from 'react';
import { 
    View,
    Text,
    TouchableOpacity
} from 'react-native';
import { connect } from 'react-redux';

import { pushBottomButtonAction } from '../../actions';
import NavigationService from '../../helpers/NavigationService';
import * as colors from '../../helpers/ColorPalette';
import * as defaults from '../../helpers/DefaultValues';
import styles from './styles';

// TODO: Başlık textInput'u için bir maxLength tanımlanacak

class PreviousWritingSmall extends Component {

    handleButtonPress = () => {
        const { pushBottomButtonAction } = this.props;

        pushBottomButtonAction(defaults.buttonNames.none);
        NavigationService.navigate('ReadPage');
    }

    render() {
        const { day, title } = this.props;
        return (
            <TouchableOpacity activeOpacity={0.7} onPress={() => this.handleButtonPress()} >
                <View style={styles.previousWritingSmallContainerStyle} >
                    <Text style={styles.previousWritingSmallTextStyle}>{title} - About {day}</Text>

                    <View style={styles.previousWritingSmallCreationDateContainerStyle} >
                        <Text>Created: 08/11/2007</Text>
                    </View>
                </View>
            </TouchableOpacity>
        );
    }
}

const mapStateToProps = (state) => {
    const { showBottomMenu } = state.bottomMenuReducer;

    return { showBottomMenu };
};

export default connect(mapStateToProps, { pushBottomButtonAction })(PreviousWritingSmall);