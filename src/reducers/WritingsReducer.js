import { 
    CHANGE_MOODS,
    CHANGE_KEYWORDS,
    CHANGE_TITLE,
    CHANGE_CONTENT,
    CHANGE_MODAL_VISIBILITY,
    RESET_WRITING_ACTION,
} from '../helpers/ActionNames.js';

const INITIAL_STATE = {
    selectedMoods: [],
    keywords: [],
    title: null,
    content: null,
    modalVisible: false
};
  
export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case CHANGE_MOODS:
            return { ...state, selectedMoods: action.payload };
        case CHANGE_KEYWORDS:
            return { ...state, keywords: action.payload };
        case CHANGE_TITLE:
            return { ...state, title: action.payload };
        case CHANGE_CONTENT:
            return { ...state, content: action.payload };
        case CHANGE_MODAL_VISIBILITY:
            return { ...state, modalVisible: action.payload };
        case RESET_WRITING_ACTION:
            return { ...state, ...INITIAL_STATE };
        default:
            return state;
    }
};