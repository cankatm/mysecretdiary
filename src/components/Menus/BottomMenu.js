import React, { Component } from 'react';
import { 
    View, 
    Text, 
    TouchableOpacity
} from 'react-native';
import { DangerZone } from 'expo';
const { Lottie } = DangerZone;
import { connect } from 'react-redux';
import Icon from 'react-native-vector-icons/Ionicons';

import NavigationService from '../../helpers/NavigationService';
import BottomMenuItem from './BottomMenuItem';
import BottomMenuCloseItem from './BottomMenuCloseItem';
import { showBottomMenuAction, showBottomButton, pushBottomButtonAction, resetBottomMenuAction, resetWritingAction } from '../../actions';
import styles from './styles';
import * as colors from '../../helpers/ColorPalette';
import * as defaults from '../../helpers/DefaultValues';

import animation1 from '../../../assets/animations/menuButton1.json';
import animation2 from '../../../assets/animations/menuButton2.json';
import animation3 from '../../../assets/animations/hamburger.json';

const bottomOffset = 120;
const offsetBetweenItems = defaults.bottomMenuSmallIconsSize + 12;

//Button çeşitleri - 'menu', 'addFromAddPage', 'addFromWritingEntryPage', 'delete', 'confirm', 'none'

class BottomMenu extends Component {
    constructor(props) {
        super(props);

        this.state = {
            menuOpenedAtLeastOnce: false,
            animation: animation3,
            animationPlayed: false,
            isDisabled: false
        }
    }

    _playAnimation = () => {

        if (!this.state.animationPlayed) {
            this.setState({ isDisabled: true });
            setTimeout(() => this.setState({ isDisabled: false }), 250);
            this.animation.play(0.0, 60.0);
            this.setState({ animationPlayed: true });
        }
        else {
            this.setState({ isDisabled: true });
            setTimeout(() => this.setState({ isDisabled: false }), 250);
            this.animation.play(60.0, 0.0);
            this.setState({ animationPlayed: false, })
        }
    };

    renderBottomMenuItems = () => {
        if (this.props.showBottomMenu) {
            return (
                <View>
                    <BottomMenuItem 
                        index={0} 
                        bottom={bottomOffset} 
                        right={defaults.marginValue + ((defaults.bottomMenuSize - defaults.bottomMenuSmallIconsSize) / 2)}
                        iconName = 'ios-home'
                        goTo='HomePage'
                        isDisabled={this.state.isDisabled}
                        _playAnimation={this._playAnimation}
                    />
                    <BottomMenuItem 
                        index={1}  
                        bottom={bottomOffset + offsetBetweenItems} 
                        right={defaults.marginValue + ((defaults.bottomMenuSize - defaults.bottomMenuSmallIconsSize) / 2)}  
                        iconName = 'ios-settings' 
                        goTo='SettingsPage'
                        isDisabled={this.state.isDisabled}
                        _playAnimation={this._playAnimation}
                    />
                </View>
            )
        } else if (!this.props.showBottomMenu && this.state.menuOpenedAtLeastOnce) { //it has no purpose but closing animation
            return (
                <View>
                    <BottomMenuCloseItem 
                        index={1} 
                        bottom={bottomOffset} 
                        right={defaults.marginValue + ((defaults.bottomMenuSize - defaults.bottomMenuSmallIconsSize) / 2)}
                        iconName = 'ios-home' 
                    />
                    <BottomMenuCloseItem 
                        index={0} 
                        bottom={bottomOffset  + offsetBetweenItems} 
                        right={defaults.marginValue + ((defaults.bottomMenuSize - defaults.bottomMenuSmallIconsSize) / 2)}
                        iconName = 'ios-settings' 
                    />
                </View>
            )
        }
    }

    handleAnimationButton = () =>  {
        this.props.showBottomMenuAction(!this.props.showBottomMenu),
        this.setState({ menuOpenedAtLeastOnce: true }),
        this._playAnimation()
    }

    handleConfirmFromKeywords = () =>  {

    }

    renderBottomButton = () => {
        const { pushBottomButtonAction, resetBottomMenuAction } = this.props;
        switch (this.props.showCurrentButton[this.props.showCurrentButton.length - 1]) {
            case defaults.buttonNames.menu:
                return (
                    <View style={styles.bottomMenuContainerStyle} >
                        <TouchableOpacity disabled={this.state.isDisabled} activeOpacity={1.0} onPress={() => this.handleAnimationButton()} >
                            <View style={styles.bottomMenuInnerContainerStyle} >

                                <View style={{ marginLeft: 16, marginTop: 15 }} >
                                    {this.state.animation &&
                                        <Lottie
                                            speed={3.5}
                                            loop={false}
                                            ref={animation => {
                                                this.animation = animation;
                                            }}
                                            style={{
                                                width: 50,
                                                height: 50,
                                            }}
                                                source={this.state.animation}
                                        />
                                    }
                                </View>

                            </View>
                        </TouchableOpacity>
                    </View>
                )
        

            case defaults.buttonNames.addFromAddPage:
                return (
                    <View style={styles.bottomMenuContainerStyle} >
                        <TouchableOpacity disabled={this.state.isDisabled} activeOpacity={0.7} onPress={() => {
                                pushBottomButtonAction(defaults.buttonNames.addFromWritingEntryPage),
                                NavigationService.navigate('WritingEntryPage')
                            }}>
                            <View style={[styles.bottomMenuInnerContainerStyle, { 
                                backgroundColor: colors.orange,
                                justifyContent: 'center',
                                alignItems: 'center'
                            }]} >
                                <Icon name='ios-add' color={colors.white} size={42} />
                            </View>
                        </TouchableOpacity>
                    </View>
                )

            case defaults.buttonNames.addFromWritingEntryPage:
                return (
                    <View style={styles.bottomMenuContainerStyle} >
                        <TouchableOpacity disabled={this.state.isDisabled} activeOpacity={0.7} onPress={() => {
                            pushBottomButtonAction(defaults.buttonNames.addFromReadPage),
                            NavigationService.navigate('ReadPage')
                        }}>
                            <View style={[styles.bottomMenuInnerContainerStyle, { 
                                backgroundColor: colors.orange,
                                justifyContent: 'center',
                                alignItems: 'center'
                            }]} >
                                <Icon name='ios-add' color={colors.white} size={42} />
                            </View>
                        </TouchableOpacity>
                    </View>
                )

            case defaults.buttonNames.addFromReadPage:
                return (
                    <View style={styles.bottomMenuContainerStyle} >
                        <TouchableOpacity disabled={this.state.isDisabled} activeOpacity={0.7} onPress={() => {
                            pushBottomButtonAction(defaults.buttonNames.addFromWritingEntryPage),
                            resetBottomMenuAction(),
                            resetWritingAction(),
                            NavigationService.navigate('HomePage')
                        }}>
                            <View style={[styles.bottomMenuInnerContainerStyle, { 
                                backgroundColor: colors.orange,
                                justifyContent: 'center',
                                alignItems: 'center'
                            }]} >
                                <Icon name='ios-add' color={colors.white} size={42} />
                            </View>
                        </TouchableOpacity>
                    </View>
                )

            case defaults.buttonNames.delete:
                return (
                    <View style={styles.bottomMenuContainerStyle} >
                        <TouchableOpacity disabled={this.state.isDisabled} activeOpacity={0.7} onPress={() => {}} >
                            <View style={[styles.bottomMenuInnerContainerStyle, { 
                                backgroundColor: colors.red,
                                justifyContent: 'center',
                                alignItems: 'center'
                            }]} >
                                <View style={{ transform: [{ rotate: '45deg' }] }} > 
                                    <Icon name='ios-add' color={colors.white} size={42} />
                                </View>
                            </View>
                        </TouchableOpacity>
                    </View>
                )

            case defaults.buttonNames.confirmFromMoodsPage:
                return (
                    <View style={styles.bottomMenuContainerStyle} >
                        <TouchableOpacity disabled={this.state.isDisabled} activeOpacity={0.7} onPress={() => {}} >
                            <View style={[styles.bottomMenuInnerContainerStyle, { 
                                backgroundColor: colors.green,
                                justifyContent: 'center',
                                alignItems: 'center'
                            }]} >
                                <Icon name='ios-checkmark' color={colors.white} size={42} />
                            </View>
                        </TouchableOpacity>
                    </View>
                )

            case defaults.buttonNames.confirmFromKeywordsPage:
                return (
                    <View style={styles.bottomMenuContainerStyle} >
                        <TouchableOpacity disabled={this.state.isDisabled} activeOpacity={0.7} onPress={() => {}} >
                            <View style={[styles.bottomMenuInnerContainerStyle, { 
                                backgroundColor: colors.green,
                                justifyContent: 'center',
                                alignItems: 'center'
                            }]} >
                                <Icon name='ios-checkmark' color={colors.white} size={42} />
                            </View>
                        </TouchableOpacity>
                    </View>
                )

            case defaults.buttonNames.none:
                return null;
        
            default:
                return null;
        }
    }

    render() {
        const { showBottomMenuAction } = this.props;

        return (
            <View style={{ position: 'absolute', bottom: 0, left: 0, width: defaults.WIDTH, flexDirection: 'row-reverse' }}>
                {
                    this.props.showBottomButton &&
                    <View>
                        <TouchableOpacity activeOpacity={1} onPress={() => this.handleAnimationButton()} >
                            <View style={{ width: this.props.showBottomMenu ? defaults.WIDTH : 0, height: this.props.showBottomMenu ? defaults.HEIGHT : 0, backgroundColor: 'rgba(0, 0, 0, 0.7)'}} />
                        </TouchableOpacity>
                        {this.renderBottomButton()}
                        {this.renderBottomMenuItems()}
                        {console.log(this.props.showCurrentButton[this.props.showCurrentButton.length - 1])}
                    </View>
                }
            </View>
        );
    }
}

const mapStateToProps = (state) => {
    const { showBottomMenu, showBottomButton, showCurrentButton } = state.bottomMenuReducer;

    return { showBottomMenu, showBottomButton, showCurrentButton };
};

export default connect(mapStateToProps, { 
    showBottomMenuAction, 
    showBottomButton, 
    pushBottomButtonAction, 
    resetBottomMenuAction, 
    resetWritingAction 
})(BottomMenu);