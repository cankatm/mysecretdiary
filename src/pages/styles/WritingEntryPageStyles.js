import { 
    StyleSheet, 
    Dimensions,
} from 'react-native';

import * as colors from '../../helpers/ColorPalette';
import * as defaults from '../../helpers/DefaultValues';

//background image size may change!!!
export default StyleSheet.create({
    writingEntryPageContainerStyle: { 
        flex: 1,
        width: defaults.WIDTH,
        height: defaults.HEIGHT,
        backgroundColor: colors.white
    },
    writingEntryPageTextInputContainerStyle: { 
        width: defaults.WIDTH - (defaults.marginValue * 4 / 3),
        height: defaults.HEIGHT - (defaults.headerHeight + (defaults.buttonHeight * 4) + (defaults.marginValue * 3)),
        marginLeft: (defaults.marginValue / 3) * 2,
        marginTop: defaults.marginValue,
        borderRadius: defaults.borderRadius,
        backgroundColor: colors.darkGrey,
        borderWidth: 1,
        borderColor: colors.orange,
        padding: defaults.marginValue,
        color: colors.darkestGrey,
        fontSize: 16
    },
    writingEntryPageTitleTextInputContainerStyle: { 
        width: defaults.WIDTH - (defaults.marginValue * 4 / 3),
        height: defaults.headerHeight,
        marginLeft: (defaults.marginValue / 3) * 2,
        marginTop: defaults.marginValue,
        borderRadius: defaults.borderRadius,
        backgroundColor: colors.darkGrey,
        borderWidth: 1,
        borderColor: colors.orange,
        paddingVertical: defaults.marginValue,
        paddingHorizontal: defaults.marginValue,
        color: colors.darkestGrey,
        fontSize: 14,
        textAlign: 'center'
    },
    checkMarkerContainerStyle: { 
        width: defaults.checkMarkerSize,
        height: defaults.checkMarkerSize,
        borderRadius: defaults.checkMarkerSize / 2,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: colors.green,
        position: 'absolute',
        right: defaults.marginValue / 2,
        top: defaults.marginValue / 2,
        zIndex: 16
    },
    writingEntryPageModalHeaderContainerStyle: { 
        width: defaults.WIDTH,
        height: defaults.headerHeight,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    },
    writingEntryPageModalHeaderIconContainerStyle: { 
        position: 'absolute',
        top: 0,
        left: 0
    },
    writingEntryPageModalHeaderIconInnerContainerStyle: { 
        width: defaults.headerHeight,
        height: defaults.headerHeight,
        alignItems: 'center',
        justifyContent: 'center'
    },
    writingEntryPageModalHeaderTextStyle: { 
        color: colors.orange,
        fontSize: 20
    },
});