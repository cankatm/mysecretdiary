import React, { Component } from 'react';
import { 
    View, 
    Text, 
    TouchableOpacity,
    Animated,
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import { connect } from 'react-redux';

import { showBottomMenuAction } from '../../actions';
import NavigationService from '../../helpers/NavigationService';
import styles from './styles';
import * as colors from '../../helpers/ColorPalette';
import * as defaults from '../../helpers/DefaultValues';

class BottomMenuItem extends Component {
    constructor(props) {
        super(props);

        this.state = {
            animateItem: new Animated.Value(0)
        }
    }

    componentWillMount() {
        const { index } = this.props;
        const delay = index * 50;

        Animated.timing(this.state.animateItem, {
            toValue: 1,
            duration: 100,
            delay
        }).start()
    }

    handleButtonPress = (goTo) => {
        this.props._playAnimation(),
        this.props.showBottomMenuAction(false),
        NavigationService.navigate(goTo)
    }

    render() {
        const { bottom, top, left, right, iconName, goTo, isDisabled } = this.props;

        return (
            <Animated.View style={[styles.bottomMenuItemContainerStyle, { bottom , left , top, right, opacity: this.state.animateItem ,transform: [{ scale: this.state.animateItem }] }]} >
                <TouchableOpacity disabled={isDisabled} onPress={() => this.handleButtonPress(goTo)} >
                    <View style={styles.bottomMenuItemInnerContainerStyle} >
                        <Icon name={iconName} size={24} color={colors.white} />
                    </View>
                </TouchableOpacity>
            </Animated.View>
        );
    }
}

const mapStateToProps = (state) => {
    const { showBottomMenu } = state.bottomMenuReducer;

    return { showBottomMenu };
};

export default connect(mapStateToProps, { showBottomMenuAction })(BottomMenuItem);