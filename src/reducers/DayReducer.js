import { 
    CHANGE_DAY 
} from '../helpers/ActionNames.js';

const INITIAL_STATE = {
    day: null
};
  
export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case CHANGE_DAY:
            return { ...state, day: action.payload };
        default:
            return state;
    }
};