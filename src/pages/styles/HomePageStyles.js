import { 
    StyleSheet, 
    Dimensions,
} from 'react-native';

import * as colors from '../../helpers/ColorPalette';
import * as defaults from '../../helpers/DefaultValues';

export default StyleSheet.create({
    homePageStylesContainerStyle: { 
        flex: 1, 
        backgroundColor: colors.white,
        width: defaults.WIDTH, 
        height: defaults.HEIGHT, 
    },
    homePageStylesIconsContainerStyle: { 
        width: defaults.WIDTH, 
        flexDirection: 'row', 
        position: 'absolute', 
        top: (defaults.HEIGHT / 2) - (defaults.homePageIconSize / 2), 
        left: 0, 
        justifyContent: 'space-between'
    },
});