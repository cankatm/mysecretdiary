import { 
    StyleSheet, 
    Dimensions,
} from 'react-native';

import * as colors from '../../helpers/ColorPalette';
import * as defaults from '../../helpers/DefaultValues';

export default StyleSheet.create({
    searchPageContainerStyle: { 
        flex: 1, 
        backgroundColor: colors.white,
        width: defaults.WIDTH, 
        height: defaults.HEIGHT, 
    },
    searchPageInnerContainerStyle: { 
        backgroundColor: colors.white,
        width: defaults.WIDTH, 
        marginTop: (defaults.HEIGHT / 2) - defaults.headerHeight - (defaults.buttonHeight * 2) - defaults.marginValue,
        alignItems: 'center',
        justifyContent: 'center'
    },
    searchPageRowContainerStyle: { 
        flexDirection: 'row', 
        alignItems: 'center', 
        justifyContent: 'space-between' 
    },
    searchPageCalendarContainerStyle: { 
        flexDirection: 'row', 
        alignItems: 'center', 
        justifyContent: 'space-between' 
    },
});