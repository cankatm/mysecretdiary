import { StyleSheet } from 'react-native';

import * as colors from '../../helpers/ColorPalette';
import * as defaults from '../../helpers/DefaultValues';

export default StyleSheet.create({
    centeredAreaContainerStyle: { 
        width: defaults.WIDTH, 
        alignItems: 'center', 
    },
});