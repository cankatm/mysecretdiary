import _ from 'lodash';
import React, { Component } from 'react';
import { 
    View,
    Text,
    FlatList,
    ScrollView,
    TouchableOpacity
} from 'react-native';
import { connect } from 'react-redux';
import Icon from 'react-native-vector-icons/Ionicons';

import { SquareButton } from '../components/Buttons';
import * as styles from './styles';
import { changeModalVisibilityAction, changeMoodsAction } from '../actions';
import * as colors from '../helpers/ColorPalette';
import * as defaults from '../helpers/DefaultValues';
import { moods } from '../helpers/StaticDatas';

class MoodPage extends Component {
    constructor(props) {
        super(props);
        this.state= {
            selected: (new Map(): Map<string, boolean>),
            filtreArr: [],
            valuesFinal: []
        }
    }

    componentWillMount() {
        const { selectedMoods } = this.props;
        this.setState({ filtreArr: selectedMoods })

        if (selectedMoods.length > 0) {
            this.setState((state) => {
                let selected = new Map(state.selected);

                for (var filtreArrItem of state.filtreArr) {
                    selected.set(filtreArrItem, true);
                }
                
                return { selected }
            });
        }
    }

    _onPressItem = (moodName: string) => {
        this.setState((state) => {
            let selected = new Map(state.selected);
            let filtreArr = state.filtreArr;
            
            if(selected.get(moodName)){
                for (var [key, value] of selected) {
                    if (filtreArr.includes(key)) {
                        _.remove(filtreArr, function(n) {
                            return n == key;
                        })
                    } 
                }

                selected.delete(moodName)
            }
            else
                selected.set(moodName, true);
            return {selected, filtreArr};});
    }

    handleVerifyButton = () => {
        const { selectedMoods, changeModalVisibilityAction, changeMoodsAction } = this.props;

        for (var [key, value] of this.state.selected) {
            this.state.filtreArr.push(key)
        }

        changeModalVisibilityAction(false);
        changeMoodsAction(_.uniq(this.state.filtreArr));
    }

    handleFiltreButton = () => {
        for (var [key, value] of this.state.selected) {
            this.state.filtreArr.push(key)
            
            let tempArr = _.uniq(this.state.filtreArr);
            this.setState({ filtreArr: tempArr })
        }

    }

    render() {
        const { selected } = this.state;
        
        return (
            <View style={styles.MoodPageStyles.moodPageStylesContainerStyle} >
                <FlatList 
                    data={moods}
                    numColumns={2}
                    keyExtractor={(item, index) => item.id}
                    extraData={selected}
                    renderItem={({item}) => (
                        <SquareButton 
                            id={item.id}
                            buttonText={item.moodName}
                            marginTop={defaults.marginValue}
                            onPressItem={this._onPressItem}
                            item={item}
                            selected={!!this.state.selected.get(item.moodName)}
                        />
                    )}
                />

                <View style={styles.KeyWordPageStyles.keywordPageStylesBottomButtonContainerStyle} >
                    <TouchableOpacity activeOpacity={0.8} onPress={() => this.handleVerifyButton()} >
                        <View style={styles.KeyWordPageStyles.keywordPageStylesBottomButtonInnerContainerStyle} >
                            <Icon name='ios-checkmark' size={42} color={colors.white} />
                        </View>
                    </TouchableOpacity>
                </View>

            </View>
        );
    }
}

const mapStateToProps = (state) => {
    const { selectedMoods } = state.writingsReducer

    return { selectedMoods };
};

export default connect(mapStateToProps, { changeModalVisibilityAction, changeMoodsAction })(MoodPage);