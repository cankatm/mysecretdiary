import React, { Component } from 'react';
import { 
    View,
    Text,
} from 'react-native';

import styles from './styles';

class MoodAndKeywordViewArea extends Component {
    render() {
        const { itemName } = this.props;
        return (
            <View style={styles.moodViewAreaContainerStyle} >
                <Text style={styles.moodViewAreaTextStyle}>{itemName}</Text>
            </View>
        );
    }
}

export default MoodAndKeywordViewArea;