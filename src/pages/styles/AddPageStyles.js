import { 
    StyleSheet, 
    Dimensions,
} from 'react-native';

import * as colors from '../../helpers/ColorPalette';
import * as defaults from '../../helpers/DefaultValues';

export default StyleSheet.create({
    addPageContainerStyle: { 
        flex: 1, 
        backgroundColor: colors.white,
        width: defaults.WIDTH, 
        height: defaults.HEIGHT, 
    },
});