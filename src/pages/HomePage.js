import React, { Component } from 'react';
import { 
    View,
    Text,
    Image,
    TouchableOpacity
} from 'react-native';
import { connect } from 'react-redux';
import Icon from 'react-native-vector-icons/Ionicons';

import { HomeAndSearchRoundedButton } from '../components/Buttons';
import { CenteredArea } from '../components/CenteredAreas';
import logoImage from '../../assets/images/mySecretDiaryLogo.png';
import { showBottomButtonAction, showBottomMenuAction, pushBottomButtonAction } from '../actions';
import * as defaults from '../helpers/DefaultValues';
import * as colors from '../helpers/ColorPalette';
import * as styles from './styles';

class HomePage extends Component {
    constructor(props) {
        super(props);
    }

    componentWillMount() {
        const { showBottomButtonAction, pushBottomButtonAction } = this.props;
        showBottomButtonAction(true);
    }

    handleAddButton = () => {
        const { navigation, showBottomMenuAction, pushBottomButtonAction } = this.props;
        showBottomMenuAction(false);

        pushBottomButtonAction(defaults.buttonNames.none);

        navigation.navigate('AddPage');
    }

    handleSearchButton = () => {
        const { navigation, showBottomMenuAction, pushBottomButtonAction } = this.props;
        showBottomMenuAction(false);

        pushBottomButtonAction(defaults.buttonNames.none);

        navigation.navigate('SearchPage');
    }

    render() {
        return (
            <View style={styles.HomePageStyles.homePageStylesContainerStyle} >
                <CenteredArea marginTop={defaults.marginValue} >
                    <Image source={logoImage} style={{ width: 120, height: 120 }} resizeMode='contain' />
                </CenteredArea>

                <View style={styles.HomePageStyles.homePageStylesIconsContainerStyle} >

                    <TouchableOpacity onPress={() => this.handleAddButton()} >
                        <HomeAndSearchRoundedButton icon iconName='ios-add' iconColor={colors.orange} iconSize={48} />
                    </TouchableOpacity>

                    <TouchableOpacity onPress={() => this.handleSearchButton()} >
                        <HomeAndSearchRoundedButton icon iconName='ios-search' iconColor={colors.orange} iconSize={36} />
                    </TouchableOpacity>

                </View>
            </View>
        );
    }
}

const mapStateToProps = (state) => {
    const { showBottomMenu } = state.bottomMenuReducer;

    return { showBottomMenu };
};

export default connect(mapStateToProps, { showBottomButtonAction, showBottomMenuAction, pushBottomButtonAction })(HomePage);