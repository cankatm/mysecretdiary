import { StyleSheet } from 'react-native';

import * as colors from '../../helpers/ColorPalette';
import * as defaults from '../../helpers/DefaultValues';

export default StyleSheet.create({
    previousWritingSmallContainerStyle: {
        width: defaults.WIDTH - (defaults.marginValue * 2),
        height: 80,
        borderColor: colors.orange,
        borderWidth: 1,
        backgroundColor: colors.lightGrey,
        borderRadius: defaults.borderRadius,
        alignItems: 'stretch',
        justifyContent: 'space-between',
        marginTop: defaults.marginValue
    },
    previousWritingSmallCreationDateContainerStyle: { 
        flex: 1, 
        alignItems: 'flex-end', 
        justifyContent: 'flex-end',
        marginRight: defaults.marginValue,
        marginBottom: defaults.marginValue,
    },
    previousWritingSmallTextStyle: {
        color: colors.orange,
        marginLeft: defaults.marginValue,
        marginTop: defaults.marginValue,
    },
});