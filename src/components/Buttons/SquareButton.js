import React, { Component } from 'react';
import { 
    View,
    Text,
    TouchableOpacity
} from 'react-native';

import styles from './styles';
import * as colors from '../../helpers/ColorPalette';
import * as defaults from '../../helpers/DefaultValues';

class SquareButton extends Component {
    static defaultProps = {
        buttonWidth: defaults.buttonWidth,
        buttonHeight: defaults.buttonHeight,
        backgroundColor: colors.darkGrey,
        borderColor: colors.orange,
        borderRadius: defaults.borderRadius,
        marginLeft: (defaults.marginValue / 3) * 2,
        marginTop: 0,
        buttonText: 'ButtonName',
        item: null
    }

    _onPress = () => {
        this.props.onPressItem(this.props.item.moodName);
    };

    render() {
        const { 
            buttonWidth, 
            buttonHeight, 
            backgroundColor, 
            borderColor, 
            borderRadius, 
            marginLeft, 
            marginTop, 
            buttonText,
            selected,
            item,
            content
        } = this.props;

        return (
            <TouchableOpacity activeOpacity={0.8} onPress={this._onPress}>
                <View style={[styles.squareButtonContainerStyle, {
                    width: buttonWidth,
                    height: buttonHeight,
                    borderColor: selected ? colors.darkGrey : colors.orange,
                    borderRadius,
                    marginLeft,
                    marginTop,
                    backgroundColor: selected ? colors.orange : colors.darkGrey
                }]} >
                    <Text style={[styles.squareButtonTextStyle, { 
                        color:  selected ? colors.white : colors.orange
                    }]} >{item.moodName}</Text>
                </View>
            </TouchableOpacity>
        );
    }
}

export default SquareButton;