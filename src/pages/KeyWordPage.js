import _ from 'lodash';
import React, { Component } from 'react';
import { 
    View,
    Text,
    TextInput,
    TouchableOpacity,
    FlatList,
    Keyboard
} from 'react-native';
import { connect } from 'react-redux';
import Icon from 'react-native-vector-icons/Ionicons';

import { KeywordButton } from '../components/Buttons';
import { CenteredArea } from '../components/CenteredAreas';
import { changeModalVisibilityAction, changeKeywordsAction } from '../actions';
import * as styles from './styles';
import * as colors from '../helpers/ColorPalette';
import * as defaults from '../helpers/DefaultValues';

class KeyWordPage extends Component {
    constructor(props) {
        super(props);

        this.state ={
            id: 0,
            keyword: null,
            keywords: []
        }
    }

    componentWillMount() {
        const { changeModalVisibilityAction, changeKeywordsAction, keywords } = this.props;

        if (keywords.length > 0) {
            this.setState({ keywords });
        }
    }

    handleAddButton = () => {
        const { keyword, keywords} = this.state;

        if (keyword) {
            Keyboard.dismiss();
            let tempKeywords = _.concat(keywords, keyword );
            this.setState({ keywords: tempKeywords, keyword: null });
        }
    }

    handleDeleteButton = (delKeyword) => {
        const { keyword, keywords} = this.state;

        someArr = keywords;

        tempArr = _.remove(someArr, function(n) {
            return n == delKeyword;
        });

        this.setState({ keywords: someArr })
    }

    handleVerifyButton = () => {
        const {  keywords } = this.state;
        console.log(keywords)
        const { changeModalVisibilityAction, changeKeywordsAction } = this.props;

        changeModalVisibilityAction(false);
        changeKeywordsAction(keywords);
    }

    render() {
        let { keyword, keywords } = this.state;

        return (
            <View style={styles.KeyWordPageStyles.keywordPageStylesContainerStyle} >
                <CenteredArea>
                    <View style={styles.KeyWordPageStyles.keywordPageStylesTextInputContainerStyle} >
                        <TextInput
                            autoCorrect={false}
                            selectionColor={colors.orange}
                            value={keyword}
                            onChangeText={(text) => this.setState({ keyword: text })}
                            style={styles.KeyWordPageStyles.keywordPageStylesTextInputStyle}
                            blurOnSubmit
                            placeholder='Write keywords here'
                        />

                        <TouchableOpacity onPress={() => this.handleAddButton()} >
                            <View style={styles.KeyWordPageStyles.keywordPageStylesConfirmButtonContainerStyle} >
                                <Text style={styles.KeyWordPageStyles.keywordPageStylesConfirmButtonTextStyle} >+</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                </CenteredArea>

                
                <FlatList 
                    data={keywords}
                    numColumns={1}
                    keyExtractor={(item, index) => item}
                    renderItem={({item}) => (
                        <CenteredArea>
                            <KeywordButton 
                                buttonText={item}
                                marginTop={defaults.marginValue}
                                handleDeleteButton={this.handleDeleteButton}
                            />
                        </CenteredArea>
                    )}
                    ListHeaderComponent={
                        keywords.length > 0 ?
                            <CenteredArea marginTop={defaults.marginValue} >
                                <Text style={styles.KeyWordPageStyles.keywordPageStylesTitleStyle} >
                                    You can see added keywords below
                                </Text>
                            </CenteredArea>
                            : null
                    }
                    ListFooterComponent={<View style={{ height: defaults.marginValue * 5}} />}
                />

                <View style={styles.KeyWordPageStyles.keywordPageStylesBottomButtonContainerStyle} >
                    <TouchableOpacity activeOpacity={0.8} onPress={() => this.handleVerifyButton()} >
                        <View style={styles.KeyWordPageStyles.keywordPageStylesBottomButtonInnerContainerStyle} >
                            <Icon name='ios-checkmark' size={42} color={colors.white} />
                        </View>
                    </TouchableOpacity>
                </View>

            </View>
        );
    }
}

const mapStateToProps = (state) => {
    const { keywords } = state.writingsReducer

    return { keywords };
};

export default connect(mapStateToProps, { changeModalVisibilityAction, changeKeywordsAction })(KeyWordPage);