import { StyleSheet } from 'react-native';

import * as colors from '../../helpers/ColorPalette';
import * as defaults from '../../helpers/DefaultValues';

export default StyleSheet.create({
    backHeaderContainerStyle: {
        width: defaults.WIDTH,
        height: defaults.headerHeight,
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: colors.transparent,
        alignItems: 'center',
        justifyContent: 'center'
    },
    backHeaderIconContainerStyle: {
        position: 'absolute',
        left: 0,
        top: 0
    },
    backHeaderHomeIconContainerStyle: {
        position: 'absolute',
        right: 0,
        top: 0
    },
    backHeaderIconInnerContainerStyle: {
        width: defaults.headerHeight,
        height: defaults.headerHeight,
        alignItems: 'center',
        justifyContent: 'center'
    },
    backHeaderTitleTextStyle: {
        color: colors.orange,
        fontSize: 20
    },
});