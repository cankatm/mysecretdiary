import React, { Component } from 'react';
import { 
    View, 
    TouchableOpacity,
    Modal,
    Text
} from 'react-native';
import { connect } from 'react-redux';
import Icon from 'react-native-vector-icons';

import { HomeAndSearchRoundedButton } from '../components/Buttons';
import { MoodPage, KeyWordPage } from './index';
import { CenteredArea } from '../components/CenteredAreas';
import { BackHeader } from '../components/Headers';
import { showBottomButtonAction, pushBottomButtonAction, changeModalVisibilityAction } from '../actions';
import * as styles from './styles';
import * as colors from '../helpers/ColorPalette';
import * as defaults from '../helpers/DefaultValues';

class SearchPage extends Component {
    constructor(props) {
        super(props);

        this.state = {
            pageName: defaults.pageNames.mood
        }
    }

    render() {
        const { pageName } = this.state;
        const { modalVisible, changeModalVisibilityAction } = this.props;

        const renderContent = () => {
            switch (pageName) {
                case defaults.pageNames.mood:
                    return (
                        <MoodPage />
                    )
            
                case defaults.pageNames.keyWords:
                    return (
                        <KeyWordPage />
                    )
            
                default:
                    return null;
            }
        }

        return (
            <View style={styles.SearchPageStyles.searchPageContainerStyle} >
                <BackHeader headerTitle='Search' />
                <View style={styles.SearchPageStyles.searchPageInnerContainerStyle} >
                    <View style={styles.SearchPageStyles.searchPageRowContainerStyle} >
                        <TouchableOpacity onPress={() => {this.setState({ pageName: defaults.pageNames.mood }), changeModalVisibilityAction(true)}} >
                            <HomeAndSearchRoundedButton text='Moods' />
                        </TouchableOpacity>

                        <TouchableOpacity>
                            <HomeAndSearchRoundedButton text='Keywords' />
                        </TouchableOpacity>
                    </View>

                    <View style={[styles.SearchPageStyles.searchPageRowContainerStyle, { marginTop: defaults.marginValue * 2}]} >
                        <TouchableOpacity>
                            <HomeAndSearchRoundedButton text='Title' />
                        </TouchableOpacity>

                        <TouchableOpacity>
                            <HomeAndSearchRoundedButton text='Day' />
                        </TouchableOpacity>
                    </View>
                </View>

                

                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={modalVisible}
                >
                    <View style={{ backgroundColor: colors.white, flex: 1}} >
                        <View style={styles.WritingEntryPageStyles.writingEntryPageModalHeaderContainerStyle} >

                            <View style={styles.WritingEntryPageStyles.writingEntryPageModalHeaderIconContainerStyle} >
                                <TouchableOpacity onPress={() => changeModalVisibilityAction(false)} >
                                    <View style={styles.WritingEntryPageStyles.writingEntryPageModalHeaderIconInnerContainerStyle} >
                                        <Icon name='ios-arrow-round-down' color={colors.orange} size={48} />
                                    </View>
                                </TouchableOpacity>
                            </View>

                            <Text style={styles.WritingEntryPageStyles.writingEntryPageModalHeaderTextStyle} >{pageName}</Text>
                        </View>

                        {renderContent()}

                    </View>
                </Modal>
            </View>
        );
    }
}

const mapStateToProps = (state) => {
    const { showBottomMenu } = state.bottomMenuReducer;
    const { modalVisible } = state.writingsReducer;

    return { showBottomMenu, modalVisible };
};

export default connect(mapStateToProps, { showBottomButtonAction, pushBottomButtonAction, changeModalVisibilityAction })(SearchPage);