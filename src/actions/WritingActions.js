import { 
    CHANGE_MOODS,
    CHANGE_KEYWORDS,
    CHANGE_TITLE,
    CHANGE_CONTENT,
    CHANGE_MODAL_VISIBILITY,
    RESET_WRITING_ACTION,
} from '../helpers/ActionNames';;

export const changeMoodsAction = (moods) => {
    return {
        type: CHANGE_MOODS,
        payload: moods
    };
}

export const changeKeywordsAction = (keywords) => {
    return {
        type: CHANGE_KEYWORDS,
        payload: keywords
    };
}

export const changeTitleAction = (title) => {
    return {
        type: CHANGE_TITLE,
        payload: title
    };
}

export const changeContentAction = (content) => {
    return {
        type: CHANGE_CONTENT,
        payload: content
    };
}

export const changeModalVisibilityAction = (content) => {
    return {
        type: CHANGE_MODAL_VISIBILITY,
        payload: content
    };
}

export const resetWritingAction = () => {
    return {
        type: RESET_WRITING_ACTION,
    };
}