import firebase from 'firebase';
require("firebase/firestore");

//API details
const config = {
    apiKey: "AIzaSyBA5NISqZirIISo1_WiQXNS3sgJsnvpqvw",
    authDomain: "mysecretdiary-c0128.firebaseapp.com",
    databaseURL: "https://mysecretdiary-c0128.firebaseio.com",
    projectId: "mysecretdiary-c0128",
    storageBucket: "mysecretdiary-c0128.appspot.com",
    messagingSenderId: "999798648067"
};

firebase.initializeApp(config);

export const f = firebase;
export const auth = firebase.auth();
export const storage = firebase.storage();

export const database = firebase.firestore();