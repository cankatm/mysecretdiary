import moment from 'moment';
import React, { Component } from 'react';
import { 
    View,
    Text,
} from 'react-native';
import { connect } from 'react-redux';

import { BackHeader } from '../components/Headers';
import { PreviousWritingSmall } from '../components/PreviousWritings';
import { CenteredArea } from '../components/CenteredAreas';
import {  showBottomButtonAction, pushBottomButtonAction } from '../actions';
import * as styles from './styles';
import * as defaults from '../helpers/DefaultValues';

const NO_DAY = 'No Day';
let formatedDay;

class ShowDayPage extends Component {

    componentWillMount() {
        const {  showBottomButtonAction } = this.props;
        showBottomButtonAction(true);
    }

    render() {
        const { day } = this.props;
        if(day) {
            formatedDay = moment(day.timestamp).format('DD/MM/YYYY');
        }

        return (
            <View style={styles.ShowDayPageStyles.showDayPageStylesContainerStyle} >
                <BackHeader headerTitle={formatedDay} />

                <CenteredArea>
                    <PreviousWritingSmall day={formatedDay} title='What a wonderful world!' />
                </CenteredArea>

                <CenteredArea>
                    <PreviousWritingSmall day={formatedDay} title='What a wonderful world!' />
                </CenteredArea>
            </View>
        );
    }
}

const mapStateToProps = (state) => {
    const { day } = state.dayReducer;

    return { day };
};

export default connect(mapStateToProps, { pushBottomButtonAction, showBottomButtonAction })(ShowDayPage);