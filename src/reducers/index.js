import { combineReducers } from 'redux';

import LanguageReducer from './LanguageReducer';
import BottomMenuReducer from './BottomMenuReducer';
import DayReducer from './DayReducer';
import WritingsReducer from './WritingsReducer';
import ImageReducer from './ImageReducer';

export default combineReducers({
    languageReducer: LanguageReducer,
    bottomMenuReducer: BottomMenuReducer,
    dayReducer: DayReducer,
    writingsReducer: WritingsReducer,
    imageReducer: ImageReducer
});