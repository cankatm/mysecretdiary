import { 
    SELECT_FIRST_IMAGE, 
    SELECT_SECOND_IMAGE, 
    SELECT_THIRD_IMAGE, 
    SELECT_FORTH_IMAGE, 
    SELECT_FIFTH_IMAGE, 
    SELECT_SIXTH_IMAGE,
    DELETE_FIRST_IMAGE,
    DELETE_SECOND_IMAGE,
    DELETE_THIRD_IMAGE,
    DELETE_FORTH_IMAGE,
    DELETE_FIFTH_IMAGE,
    DELETE_SIXTH_IMAGE,
    CHANGE_INFORMATION_TEXT
} from '../helpers/ActionNames.js';

export const selectFirstImage = (image) => {
    return { type: SELECT_FIRST_IMAGE, payload: image };
}

export const selectSecondImage = (image) => {
    return { type: SELECT_SECOND_IMAGE, payload: image };
}

export const selectThirdImage = (image) => {
    return { type: SELECT_THIRD_IMAGE, payload: image };
}

export const selectForthImage = (image) => {
    return { type: SELECT_FORTH_IMAGE, payload: image };
}

export const selectFifthImage = (image) => {
    return { type: SELECT_FIFTH_IMAGE, payload: image };
}

export const selectSixthImage = (image) => {
    return { type: SELECT_SIXTH_IMAGE, payload: image };
}



export const deleteFirstImage = () => {
    return { type: DELETE_FIRST_IMAGE };
}

export const deleteSecondImage = () => {
    return { type: DELETE_SECOND_IMAGE };
}

export const deleteThirdImage = () => {
    return { type: DELETE_THIRD_IMAGE };
}

export const deleteForthImage = () => {
    return { type: DELETE_FORTH_IMAGE, };
}

export const deleteFifthImage = () => {
    return { type: DELETE_FIFTH_IMAGE };
}

export const deleteSixthImage = () => {
    return { type: DELETE_SIXTH_IMAGE };
}


export const changeInformationText = (info) => {
    return { type: CHANGE_INFORMATION_TEXT, payload: info };
}