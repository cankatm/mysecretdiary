import { Dimensions } from 'react-native';

export const WIDTH = Dimensions.get('window').width;
export const HEIGHT = Dimensions.get('window').height;

export const signPageLogoSize = 100;
export const signPageIconContainerWidth = 48;
export const signPageTextInputContainerSize = 40;

export const loginButtonHeight = 60;
export const loginButtonWidth = 160;

export const homePageIconSize = 120;

export const headerHeight = 60;

export const buttonWidth = (WIDTH / 2) - 16;
export const buttonHeight = 60

export const checkMarkerSize = 16;

export const deleteButtonSize = 24;

export const buttonNames = {
    menu: 'menu',
    addFromAddPage: 'addFromAddPage',
    addFromWritingEntryPage: 'addFromWritingEntryPage',
    addFromReadPage: 'addFromReadPage',
    delete: 'delete',
    confirmFromMoodsPage: 'confirmFromMoodsPage',
    confirmFromKeywordsPage: 'confirmFromKeywordsPage',
    none: 'none'
}

export const pageNames = {
    mood: 'Moods',
    keyWords: 'Keywords',
    title: 'Title'
}

export const borderRadius = 12;
export const marginValue = 16;

export const bottomMenuSize = 80;
export const bottomMenuSmallIconsSize = 50;
