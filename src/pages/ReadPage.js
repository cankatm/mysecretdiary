import moment from 'moment';
import React, { Component } from 'react';
import { 
    View,
    Text,
    ScrollView
} from 'react-native';
import { connect } from 'react-redux';

import { BackHeader } from '../components/Headers';
import { MoodAndKeywordViewArea } from '../components/ViewAreas';
import { CenteredArea } from '../components/CenteredAreas';
import * as styles from './styles';
import * as colors from '../helpers/ColorPalette';
import * as defaults from '../helpers/DefaultValues';

class ReadPage extends Component {
    static defaultProps =  {
        title: 'Başlık Başa Kuzgun Leşe',
        content: 'Bacon ipsum dolor amet doner venison ham hock sausage cupim jerky shankle bresaola. Kevin ground round tri-tip, pancetta beef ribs kielbasa boudin tail chuck ball tip landjaeger pork. Rump drumstick bresaola pork short ribs tongue doner hamburger swine meatball beef burgdoggen sausage pork chop. Turducken spare ribs picanha, meatloaf ham hock pork loin pancetta jowl beef kielbasa. Short ribs biltong burgdoggen tongue. Turducken meatloaf pastrami cupim. Meatball andouille sausage, bresaola boudin drumstick short ribs.',
        selectedMoods: ['Angry', 'Happy', 'Dissapointed'],
        keywords: ['Mert', 'Bey', 'Diyeceksiniz']
    }

    renderItems = (moodOrKeywordArray) => {
        return (
            moodOrKeywordArray.map((element) => {
                return <MoodAndKeywordViewArea key={element} itemName={element} />
            })
        )
    }

    render() {
        const { day, title, content, selectedMoods, keywords } = this.props;

        if(day)
            formatedDay = moment(day.timestamp).format('DD/MM/YYYY');

        return (
            <View style={styles.ReadPageStyles.readPageStylesContainerStyle} >
                <BackHeader headerTitle={formatedDay} />

                <ScrollView>
                    
                    <CenteredArea>
                        <View style={styles.ReadPageStyles.readPageStylesTitleContainerStyle} >
                            <Text style={styles.ReadPageStyles.readPageStylesTitleTextStyle}>{title}</Text>
                        </View>
                    </CenteredArea>

                    <CenteredArea>
                        <View style={styles.ReadPageStyles.readPageStylesContentContainerStyle} >
                            <Text style={styles.ReadPageStyles.readPageStylesContentTextStyle}>{content}</Text>
                        </View>
                    </CenteredArea>

                    <View style={styles.ReadPageStyles.readPageStylesMoodAndKeywordTitleContainerStyle} >
                        <Text style={styles.ReadPageStyles.readPageStylesMoodAndKeywordTitleTextStyle}>Mood(s):</Text>
                    </View>

                    <CenteredArea>
                        <View style={styles.ReadPageStyles.readPageStylesMoodAndKeywordContainerStyle} >
                            {this.renderItems(selectedMoods)}
                        </View>
                    </CenteredArea>

                    <View style={styles.ReadPageStyles.readPageStylesMoodAndKeywordTitleContainerStyle} >
                        <Text style={styles.ReadPageStyles.readPageStylesMoodAndKeywordTitleTextStyle}>Keyword(s):</Text>
                    </View>

                    <CenteredArea>
                        <View style={styles.ReadPageStyles.readPageStylesMoodAndKeywordContainerStyle} >
                            {this.renderItems(keywords)}
                        </View>
                    </CenteredArea>

                    <View style={{ height: defaults.buttonHeight + (defaults.marginValue * 3 )}} />

                </ScrollView>

            </View>
        );
    }
}

const mapStateToProps = (state) => {
    const { day } = state.dayReducer;
    const { title, content, selectedMoods, keywords } = state.writingsReducer

    return { day, title, content, selectedMoods, keywords };
};

export default connect(mapStateToProps)(ReadPage);