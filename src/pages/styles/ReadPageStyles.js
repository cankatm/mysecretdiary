import { 
    StyleSheet, 
    Dimensions,
} from 'react-native';

import * as colors from '../../helpers/ColorPalette';
import * as defaults from '../../helpers/DefaultValues';

export default StyleSheet.create({
    readPageStylesContainerStyle: { 
        backgroundColor: colors.white, 
        width: defaults.WIDTH, 
        height: defaults.HEIGHT
    },
    readPageStylesMoodAndKeywordTitleContainerStyle: { 
        marginLeft: defaults.marginValue,
        marginVertical: defaults.marginValue
    },
    readPageStylesMoodAndKeywordTitleTextStyle: { 
        fontSize: 18,
        color: colors.orange
    },
    readPageStylesMoodAndKeywordContainerStyle: { 
        flexDirection: 'row',
        width: defaults.WIDTH - (defaults.marginValue * 2),
        flexWrap: 'wrap'
    },
    readPageStylesTitleContainerStyle: { 
        width: defaults.WIDTH - (defaults.marginValue * 2),
        flexWrap: 'wrap',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: defaults.marginValue
    },
    readPageStylesTitleTextStyle: { 
        fontSize: 24,
        color: colors.orange,
        textAlign: 'center'
    },
    readPageStylesContentContainerStyle: { 
        width: defaults.WIDTH - (defaults.marginValue * 2),
        marginTop: defaults.marginValue
    },
    readPageStylesContentTextStyle: { 
        fontSize: 14,
        color: colors.darkestGrey
    },
});