export const moods = [
    { id: '1', moodName: 'Excited'},
    { id: '2', moodName: 'Happy'},
    { id: '3', moodName: 'Relaxed'},
    { id: '4', moodName: 'Calm'},
    { id: '5', moodName: 'Bored'},
    { id: '6', moodName: 'Depressed'},
    { id: '7', moodName: 'Sad'},
    { id: '8', moodName: 'Upset'},
    { id: '9', moodName: 'Stressed'},
    { id: '10', moodName: 'Nervous'},
    { id: '11', moodName: 'Angry'},
]