export const white = '#fff';
export const black = '#000';
export const pink = '#d12e96';

export const blue = '#467de0';
export const darkGrey = '#f5f5f5';
export const darkerGrey = '#ccc';
export const darkestGrey = '#333';


export const orange = '#FC7F09';
export const green = '#009900';
export const red = '#b20000';
export const darkBlue = '#353f4f';
export const lightBlue = '#5b7384';
//export const darkBlue = '#182a3f';
//export const darkBlue = '#536878';
export const lightGrey = '#eee';
export const facebookBlue = '#3b5998';

export const transparent = 'transparent';