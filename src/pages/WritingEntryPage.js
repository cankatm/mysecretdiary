import moment from 'moment';
import React, { Component } from 'react';
import { 
    View,
    Text,
    TouchableOpacity,
    TextInput,
    Modal,
    TouchableHighlight
} from 'react-native';
import { connect } from 'react-redux';
import Icon from 'react-native-vector-icons/Ionicons';

import { BackHeader } from '../components/Headers';
import { SquareButtonWithoutTouch } from '../components/Buttons';
import { showBottomButtonAction, pushBottomButtonAction, changeModalVisibilityAction, changeTitleAction, changeContentAction } from '../actions';
import { MoodPage, KeyWordPage } from './index';
import * as styles from './styles';
import * as colors from '../helpers/ColorPalette';
import * as defaults from '../helpers/DefaultValues';
import { CenteredArea } from '../components/CenteredAreas';

let formatedDay;

class WritingEntryPage extends Component {
    constructor(props) {
        super(props);

        this.state = {
            textInputValue: null,
            title: null,
            modalVisible: true,
            pageName: defaults.pageNames.mood
        }
    }

    CheckMarker = (value, marginValue, isNotArray) => {
        if (value && isNotArray) {
            return (
                <View style={[styles.WritingEntryPageStyles.checkMarkerContainerStyle, {
                    right: marginValue ? defaults.marginValue : defaults.marginValue / 2
                }]} >
                    <Icon name='ios-checkmark' size={defaults.checkMarkerSize} color={colors.white} />
                </View>
            )
        }
        if (!isNotArray && value.length > 0) {
            return (
                <View style={[styles.WritingEntryPageStyles.checkMarkerContainerStyle, {
                    right: marginValue ? defaults.marginValue : defaults.marginValue / 2
                }]} >
                    <Icon name='ios-checkmark' size={defaults.checkMarkerSize} color={colors.white} />
                </View>
            )
        }
    }

    render() {
        const { pageName } = this.state;

        const { 
            day, 
            title, 
            content,
            selectedMoods, 
            keywords, 
            modalVisible, 
            changeModalVisibilityAction, 
            changeTitleAction ,
            changeContentAction,
            pushBottomButtonAction,
        } = this.props;

        if(day) {
            formatedDay = moment(day.timestamp).format('DD/MM/YYYY');
        }

        const renderContent = () => {
            switch (pageName) {
                case defaults.pageNames.mood:
                    return (
                        <MoodPage />
                    )
            
                case defaults.pageNames.keyWords:
                    return (
                        <KeyWordPage />
                    )
            
                case defaults.pageNames.title:
                    return (
                        <TextInput 
                            autoCorrect={false}
                            selectionColor={colors.orange}
                            value={title}
                            onChangeText={(text) => changeTitleAction(text)}
                            style={styles.WritingEntryPageStyles.writingEntryPageTitleTextInputContainerStyle}
                            onSubmitEditing={() => changeModalVisibilityAction(false)}
                            blurOnSubmit
                            placeholder='Write Title Here'
                        />
                    )
            
                default:
                    return null;
            }
        }

        return (

            <View style={styles.WritingEntryPageStyles.writingEntryPageContainerStyle} >
                {console.log(title)}
                <BackHeader headerTitle={formatedDay} />

                <View style={{ flexDirection: 'row' }} >
                    <TouchableOpacity onPress={() => {
                        this.setState({ pageName: defaults.pageNames.mood })
                        changeModalVisibilityAction(true)
                    }}>
                        <View>
                            {this.CheckMarker(selectedMoods, null, false)}
                            <SquareButtonWithoutTouch buttonText='Moods' />
                        </View>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={() => {
                        this.setState({ pageName: defaults.pageNames.keyWords })
                        changeModalVisibilityAction(true)
                    }} >
                        <View>
                            {this.CheckMarker(keywords, null, false)}
                            <SquareButtonWithoutTouch buttonText='Keywords' />
                        </View>
                    </TouchableOpacity>
                </View>

                <TouchableOpacity onPress={() => {
                    this.setState({ pageName: defaults.pageNames.title })
                    changeModalVisibilityAction(true)
                }} >
                    <View style={{ marginTop: defaults.marginValue }} >
                        {this.CheckMarker(title, defaults.marginValue, true)}
                        <SquareButtonWithoutTouch 
                            buttonWidth={defaults.WIDTH - (defaults.marginValue * 4 / 3)} 
                            buttonText='Title' 
                        />
                    </View>
                </TouchableOpacity>

                <TextInput 
                    multiline
                    autoCorrect={false}
                    selectionColor={colors.orange}
                    value={content}
                    onChangeText={(text) => changeContentAction(text)}
                    style={styles.WritingEntryPageStyles.writingEntryPageTextInputContainerStyle}
                    blurOnSubmit
                />


                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={modalVisible}
                >
                    <View style={{ backgroundColor: colors.white, flex: 1}} >
                        <View style={styles.WritingEntryPageStyles.writingEntryPageModalHeaderContainerStyle} >

                            <View style={styles.WritingEntryPageStyles.writingEntryPageModalHeaderIconContainerStyle} >
                                <TouchableOpacity onPress={() => changeModalVisibilityAction(false)} >
                                    <View style={styles.WritingEntryPageStyles.writingEntryPageModalHeaderIconInnerContainerStyle} >
                                        <Icon name='ios-arrow-round-down' color={colors.orange} size={48} />
                                    </View>
                                </TouchableOpacity>
                            </View>

                            <Text style={styles.WritingEntryPageStyles.writingEntryPageModalHeaderTextStyle} >{pageName}</Text>
                        </View>

                        {renderContent()}

                    </View>
                </Modal>

            </View>
        );
    }
}

const mapStateToProps = (state) => {
    const { day } = state.dayReducer;
    const { title, content, selectedMoods, keywords, modalVisible } = state.writingsReducer

    return { day, title, content, selectedMoods, keywords, modalVisible };
};

export default connect(mapStateToProps, { 
    showBottomButtonAction, 
    pushBottomButtonAction, 
    changeModalVisibilityAction, 
    changeTitleAction,
    changeContentAction
})(WritingEntryPage);;