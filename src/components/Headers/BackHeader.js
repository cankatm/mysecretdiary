import React, { Component } from 'react';
import { 
    View,
    Text,
    TouchableOpacity
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import { connect } from 'react-redux';
import { withNavigation } from 'react-navigation';

import NavigationService from '../../helpers/NavigationService';
import { popBottomButtonAction, resetBottomMenuAction, resetWritingAction } from '../../actions';
import styles from './styles';
import * as colors from '../../helpers/ColorPalette';

class BackHeader extends Component {
    render() {
        const { 
            navigation, 
            popBottomButtonAction, 
            resetBottomMenuAction, 
            resetWritingAction, 
            headerTitle 
        } = this.props;

        const handleBackButton = () => {
            popBottomButtonAction(),
            navigation.goBack()
        }

        const handleHomeButton = () => {
            NavigationService.reset('HomePage'),

            resetBottomMenuAction(),
            resetWritingAction()
        }

        return (
            <View style={styles.backHeaderContainerStyle} >
                <View style={styles.backHeaderIconContainerStyle} >
                    <TouchableOpacity onPress={() => handleBackButton()} >
                        <View style={styles.backHeaderIconInnerContainerStyle}>
                            <Icon name='ios-arrow-round-back' size={48} color={colors.orange} />
                        </View>
                    </TouchableOpacity>
                </View>

                <Text style={styles.backHeaderTitleTextStyle} >{headerTitle}</Text>
                
                <View style={styles.backHeaderHomeIconContainerStyle} >
                    <TouchableOpacity onPress={() => handleHomeButton()} >
                        <View style={styles.backHeaderIconInnerContainerStyle}>
                            <Icon name='md-home' size={28} color={colors.orange} />
                        </View>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

const mapStateToProps = (state) => {
    const { day } = state.dayReducer;
    const { title, content, selectedMoods, keywords } = state.writingsReducer

    return { day, title, content, selectedMoods, keywords };
};

export default withNavigation(connect(mapStateToProps, { popBottomButtonAction, resetBottomMenuAction, resetWritingAction })(BackHeader));