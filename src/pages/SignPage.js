import React, { Component } from 'react';
import { 
    View,
    Text,
    ImageBackground,
    Image,
    TouchableOpacity,
    TextInput
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

import { f, auth, database } from '../config/config';
import { CenteredArea } from '../components/CenteredAreas';
import * as styles from './styles';
import * as colors from '../helpers/ColorPalette';
import * as defaults from '../helpers/DefaultValues';

import backgroundImage from '../../assets/images/signBackground.png';
import logoImage from '../../assets/images/Logo1.png';

class SignPage extends Component {
    constructor(props) {
        super(props);

        this.state = {
            showLoginItems: true,
            showLoginPassword: false,
            loginEmail: 'm@m.com',
            loginPassword: 'aaaaaa',
            signUpEmail: null,
            signUpPassword: null,
            signUpPasswordRepeat: null,
            showSignUpPassword: false,
            showSignUpPasswordRepeat: false,
        }
    }

    render() {
        const { 
            showLoginItems, 
            showLoginPassword, 
            loginEmail, 
            loginPassword, 
            signUpEmail, 
            signUpPassword, 
            signUpPasswordRepeat ,
            showSignUpPassword,
            showSignUpPasswordRepeat
        } = this.state;

        const { navigation } = this.props;

        const handleLoginButtonPress = () => {
            loginUser();
        }

        const loginUser = async() => {
            if (loginEmail != '' && loginPassword != '') {
                auth.signInWithEmailAndPassword(loginEmail, loginPassword)
                    .then((user) => navigation.navigate('HomePage', { user }))
                    .catch((error) => alert('giriş başarısız'))
            } else {
                alert('Missing email or password')
            }
        }

        const handleSignUpButtonPress = () => {
            registerUser();
        }

        const registerUser = () => {
            auth.createUserWithEmailAndPassword(signUpEmail, signUpPassword)
                .then((user) => { 
                    f.firestore().settings({
                        timestampsInSnapshots: true
                    })

                    f.firestore().collection('users').doc(user.user.uid).set({
                        uid: user.user.uid
                    })
                })
                .then((user) => navigation.navigate('SearchAndLocationPage', { user }))
                .catch((error) => console.log('error logging in', error))
        }

        const renderLoginOrSignButton = () => {
            if (showLoginItems) {
                return (
                    <TouchableOpacity onPress={() => handleLoginButtonPress()} >
                        <View style={styles.SignPageStyles.signPageLoginButtonInnerContainerStyle} >
                            <Text style={styles.SignPageStyles.signPageLoginButtonTextStyle} >LOGIN</Text>
                        </View>
                    </TouchableOpacity>
                )
            }

            return (
                <TouchableOpacity onPress={() => handleSignUpButtonPress()} >
                    <View style={styles.SignPageStyles.signPageLoginButtonInnerContainerStyle} >
                        <Text style={styles.SignPageStyles.signPageLoginButtonTextStyle} >SIGN UP</Text>
                    </View>
                </TouchableOpacity>
            )
        }

        const renderLoginOrSignCard = () => {
            if (showLoginItems) {
                return (
                    <View style={styles.SignPageStyles.signPageCardContainerStyle} >
    
                        <View style={styles.SignPageStyles.signPageTextInputContainerStyle} >
                            <View style={styles.SignPageStyles.signPageTextInputIconContainerStyle} >
                                <Icon name='ios-person' color={colors.orange} size={24} />
                            </View>
                            <TextInput 
                                style={styles.SignPageStyles.signPageTextInputStyle}
                                onChangeText={(email) => this.setState({ loginEmail: email })}
                                value={loginEmail}
                                keyboardType='email-address'
                                maxLength={50}
                                placeholder='your@mail.com'
                                placeholderTextColor={colors.lightBlue}
                                selectionColor={colors.orange}
                            />
                        </View>
                        
                        <View style={styles.SignPageStyles.signPageTextInputContainerStyle} >
                            <View style={styles.SignPageStyles.signPageTextInputIconContainerStyle} >
                                <Icon name='ios-lock' color={colors.orange} size={24} />
                            </View>
                            <TextInput 
                                style={[styles.SignPageStyles.signPageTextInputStyle, { 
                                    width: ((defaults.WIDTH / 3) * 2) - defaults.signPageIconContainerWidth * 2,
                                    borderTopRightRadius: 0,
                                    borderBottomRightRadius: 0
                                }]}
                                secureTextEntry={!showLoginPassword}
                                onChangeText={(password) => this.setState({ loginPassword: password })}
                                value={loginPassword}
                                maxLength={50}
                                placeholder='password'
                                placeholderTextColor={colors.lightBlue}
                                selectionColor={colors.orange}
                            />
                            
                            <TouchableOpacity onPress={() => this.setState({ showLoginPassword: !showLoginPassword })} >
                                <View style={styles.SignPageStyles.signPageTextInputIconContainerStyle} >
                                    {
                                        !showLoginPassword ? 
                                        <Icon name='ios-eye' color={colors.orange} size={24} /> :
                                        <Icon name='ios-eye-off' color={colors.orange} size={24} />
                                    }
                                </View>
                            </TouchableOpacity>
                        </View>
    
                        <View style={styles.SignPageStyles.signPageForgotPasswordAndSignupContainerStyle} >
                            <TouchableOpacity>
                                <View style={styles.SignPageStyles.signPageForgotPasswordAndSignupButtonContainerStyle} >
                                    <Text style={styles.SignPageStyles.signPageForgotPasswordAndSignupButtonTextStyle} >Forgot Password</Text>
                                </View>
                            </TouchableOpacity>
    
                            <TouchableOpacity onPress={() => this.setState({ showLoginItems: false })} >
                                <View style={styles.SignPageStyles.signPageForgotPasswordAndSignupButtonContainerStyle} >
                                    <Text style={styles.SignPageStyles.signPageForgotPasswordAndSignupButtonTextStyle}>Sign Up</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                    </View>
                )
            }

            return (
                <View style={styles.SignPageStyles.signPageCardContainerStyle} >
                
                    <View style={styles.SignPageStyles.signPageTextInputContainerStyle} >
                        <View style={styles.SignPageStyles.signPageTextInputIconContainerStyle} >
                            <Icon name='ios-person' color={colors.orange} size={24} />
                        </View>
                        <TextInput 
                            style={styles.SignPageStyles.signPageTextInputStyle}
                            onChangeText={(email) => this.setState({ signUpEmail: email })}
                            value={signUpEmail}
                            keyboardType='email-address'
                            maxLength={50}
                            placeholder='your@mail.com'
                            placeholderTextColor={colors.lightBlue}
                            selectionColor={colors.orange}
                        />
                    </View>
                    
                    <View style={styles.SignPageStyles.signPageTextInputContainerStyle} >
                        <View style={styles.SignPageStyles.signPageTextInputIconContainerStyle} >
                            <Icon name='ios-lock' color={colors.orange} size={24} />
                        </View>
                        <TextInput 
                            style={[styles.SignPageStyles.signPageTextInputStyle, { 
                                width: ((defaults.WIDTH / 3) * 2) - defaults.signPageIconContainerWidth * 2,
                                borderTopRightRadius: 0,
                                borderBottomRightRadius: 0
                            }]}
                            secureTextEntry={!showSignUpPassword}
                            onChangeText={(password) => this.setState({ signUpPassword: password })}
                            value={signUpPassword}
                            maxLength={50}
                            placeholder='password'
                            placeholderTextColor={colors.lightBlue}
                            selectionColor={colors.orange}
                        />
                        
                        <TouchableOpacity onPress={() => this.setState({ showSignUpPassword: !showSignUpPassword })} >
                            <View style={styles.SignPageStyles.signPageTextInputIconContainerStyle} >
                                {
                                    !showSignUpPassword ? 
                                    <Icon name='ios-eye' color={colors.orange} size={24} /> :
                                    <Icon name='ios-eye-off' color={colors.orange} size={24} />
                                }
                            </View>
                        </TouchableOpacity>
                    </View>
                    
                    <View style={styles.SignPageStyles.signPageTextInputContainerStyle} >
                        <View style={styles.SignPageStyles.signPageTextInputIconContainerStyle} >
                            <Icon name='ios-lock' color={colors.orange} size={24} />
                        </View>
                        <TextInput 
                            style={[styles.SignPageStyles.signPageTextInputStyle, { 
                                width: ((defaults.WIDTH / 3) * 2) - defaults.signPageIconContainerWidth * 2,
                                borderTopRightRadius: 0,
                                borderBottomRightRadius: 0
                            }]}
                            secureTextEntry={!showSignUpPasswordRepeat}
                            onChangeText={(password) => this.setState({ signUpPasswordRepeat: password })}
                            value={signUpPasswordRepeat}
                            maxLength={50}
                            placeholder='password'
                            placeholderTextColor={colors.lightBlue}
                            selectionColor={colors.orange}
                        />
                        
                        <TouchableOpacity onPress={() => this.setState({ showSignUpPasswordRepeat: !showSignUpPasswordRepeat })} >
                            <View style={styles.SignPageStyles.signPageTextInputIconContainerStyle} >
                                {
                                    !showSignUpPasswordRepeat ? 
                                    <Icon name='ios-eye' color={colors.orange} size={24} /> :
                                    <Icon name='ios-eye-off' color={colors.orange} size={24} />
                                }
                            </View>
                        </TouchableOpacity>
                    </View>

                    <View style={styles.SignPageStyles.signPageForgotPasswordAndSignupContainerStyle} >
                        <TouchableOpacity>
                            <View style={styles.SignPageStyles.signPageForgotPasswordAndSignupButtonContainerStyle} >
                                <Text style={styles.SignPageStyles.signPageForgotPasswordAndSignupButtonTextStyle} >Forgot Password</Text>
                            </View>
                        </TouchableOpacity>

                        <TouchableOpacity onPress={() => this.setState({ showLoginItems: true })} >
                            <View style={styles.SignPageStyles.signPageForgotPasswordAndSignupButtonContainerStyle} >
                                <Text style={styles.SignPageStyles.signPageForgotPasswordAndSignupButtonTextStyle}>Login</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                </View>
            )
        }

        return (
            <View style={{ flex: 1, backgroundColor: colors.white }} >
                <ImageBackground resizeMode='cover' source={backgroundImage} style={styles.SignPageStyles.signPageContainerImageStyle} > 
                    <CenteredArea marginTop={64} >
                        <View style={styles.SignPageStyles.signPageLogoImageContainerStyle}>
                            <Image resizeMode='contain' source={logoImage} style={styles.SignPageStyles.signPageLogoImageStyle}  />
                        </View>
                    </CenteredArea>
                </ImageBackground>

                {renderLoginOrSignCard()}

                <View style={styles.SignPageStyles.signPageLoginButtonContainerStyle} >
                    {renderLoginOrSignButton()}
                </View>
            </View>
        );
    }
}

export default SignPage;