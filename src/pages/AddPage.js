import React, { Component } from 'react';
import { 
    View, 
    Text,
    TouchableOpacity,
    AppState
} from 'react-native';
import { connect } from 'react-redux';
import { Calendar, CalendarList, Agenda } from 'react-native-calendars';

import { BackHeader } from '../components/Headers';
import { showBottomButtonAction, pushBottomButtonAction, changeDayAction } from '../actions';
import * as styles from './styles';
import * as colors from '../helpers/ColorPalette';
import * as defaults from '../helpers/DefaultValues';

class AddPage extends Component {
    constructor(props) {
        super(props);

        this.state = {
            appState: AppState.currentState
        }
    }

    componentWillMount() {
        const { showBottomButtonAction } = this.props;
        showBottomButtonAction(true);
    }

    handleDayPress = (day) => {
        const { pushBottomButtonAction, changeDayAction, navigation } = this.props;

        pushBottomButtonAction(defaults.buttonNames.addFromAddPage);
        changeDayAction(day);
        navigation.navigate('ShowDayPage');
    }

    render() {
        return (
            <View style={styles.AddPageStyles.addPageContainerStyle} >
                <BackHeader headerTitle={'Calendar'} />
                
                <Calendar 
                    firstDay={1}
                    onDayPress= {(day) => this.handleDayPress(day)}
                    theme={calendarTheme}
                />
            </View>
        );
    }
}

const calendarTheme = {
    backgroundColor: '#ffffff',
    calendarBackground: '#ffffff',
    textSectionTitleColor: '#b6c1cd',
    selectedDayBackgroundColor: colors.orange,
    selectedDayTextColor: '#ffffff',
    todayTextColor: colors.orange,
    dayTextColor: '#2d4150',
    textDisabledColor: '#d9e1e8',
    dotColor: '#00adf5',
    selectedDotColor: '#ffffff',
    arrowColor: colors.orange,
    monthTextColor: colors.orange,
    textMonthFontWeight: 'bold',
    textDayFontSize: 14,
    textMonthFontSize: 16,
    textDayHeaderFontSize: 16,
}

const mapStateToProps = (state) => {
    const { showBottomMenu } = state.bottomMenuReducer;

    return { showBottomMenu };
};

export default connect(mapStateToProps, { showBottomButtonAction, pushBottomButtonAction, changeDayAction })(AddPage);