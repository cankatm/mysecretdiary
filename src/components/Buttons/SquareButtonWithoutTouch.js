import React, { Component } from 'react';
import { 
    View,
    Text,
    TouchableOpacity
} from 'react-native';

import styles from './styles';
import * as colors from '../../helpers/ColorPalette';
import * as defaults from '../../helpers/DefaultValues';

class SquareButtonWithoutTouch extends Component {
    static defaultProps = {
        buttonWidth: defaults.buttonWidth,
        buttonHeight: defaults.buttonHeight,
        backgroundColor: colors.darkGrey,
        borderColor: colors.orange,
        borderRadius: defaults.borderRadius,
        marginLeft: (defaults.marginValue / 3) * 2,
        marginTop: 0,
        buttonText: 'ButtonName'
    }

    render() {
        const { 
            buttonWidth, 
            buttonHeight, 
            backgroundColor, 
            borderColor, 
            borderRadius, 
            marginLeft, 
            marginTop, 
            buttonText,
        } = this.props;

        return (
            <View style={[styles.squareButtonContainerStyle, {
                width: buttonWidth,
                height: buttonHeight,
                backgroundColor,
                borderColor,
                borderRadius,
                marginLeft,
                marginTop,
                backgroundColor: colors.darkGrey
            }]} >
                <Text style={styles.squareButtonTextStyle} >{buttonText}</Text>
            </View>
        );
    }
}

export default SquareButtonWithoutTouch;