import React, { Component } from 'react';
import { 
    View,
    Text,
    TouchableOpacity,
    Animated
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

import styles from './styles';
import * as colors from '../../helpers/ColorPalette';
import * as defaults from '../../helpers/DefaultValues';

const durationTime = 300;

class KeywordButton extends Component {
    constructor(props) {
        super(props);

        this.state = {
            animation: new Animated.Value(0),
        }
    }

    static defaultProps = {
        buttonWidth: defaults.WIDTH - (defaults.marginValue * 2),
        buttonHeight: defaults.buttonHeight,
        backgroundColor: colors.darkGrey,
        borderColor: colors.orange,
        borderRadius: defaults.borderRadius,
        marginTop: 0,
        buttonText: 'ButtonName'
    }

    componentWillMount() {
        this.handleAnimation();
    }

    handleAnimation = () => {
        Animated.timing(this.state.animation, {
            toValue: 1,
            duration: durationTime
        }).start();
    }
    
    deleteButtonPress = () => {
        Animated.timing(this.state.animation, {
            toValue: 0,
            duration: durationTime
        }).start(() => {
            this.props.handleDeleteButton(this.props.buttonText);
        })
    }

    renderDeleteButton = () => {
        return (
            <TouchableOpacity onPress={() => this.deleteButtonPress()} >
                <View style={styles.keywordButtonDeleteButtonContainerStyle} >
                    <Icon name='ios-trash' size={(defaults.deleteButtonSize / 3) * 2} color={colors.white} />
                </View>
            </TouchableOpacity>
        )
    }

    render() {
        const { 
            buttonWidth,
            buttonHeight,
            backgroundColor,
            borderColor,
            borderRadius,
            marginTop,
            buttonText,
        } = this.props;
        

        const animatedSearchAreaStyle = { opacity: this.state.animation }

        return (
            <Animated.View style={[styles.keywordButtonContainerStyle, animatedSearchAreaStyle, {
                    width: buttonWidth,
                    height: buttonHeight,
                    backgroundColor,
                    borderColor,
                    borderRadius,
                    marginTop,
                    backgroundColor
            }]}>
                <View style={styles.keywordButtonTextContainerStyle} >
                    <Text style={styles.keywordButtonTextStyle}>{buttonText}</Text>
                </View>

                {this.renderDeleteButton()}
            </Animated.View>
        );
    }
}

export default KeywordButton;