import React, { Component } from 'react';
import { 
    View,
    Text,
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

import styles from './styles';
import * as colors from '../../helpers/ColorPalette';
import * as defaults from '../../helpers/DefaultValues';

class HomeAndSearchRoundedButton extends Component {
    render() {
        const { text, img, icon, iconName, iconColor, iconSize } = this.props;

        const renderImageOrIcon = () => {
            if (icon) {
                return <Icon name={iconName} color={iconColor} size={iconSize} />;
            }
            if (text) {
                return <Text style={styles.homeAndSearchRoundedButtonTextStyle} >{text}</Text>;
            }
        }

        return (
            <View style={styles.homeAndSearchRoundedButtonShadowStyle}>
                <View style={styles.homeAndSearchRoundedButtonContainerStyle} >
                    {renderImageOrIcon()}
                </View>
            </View>
        );
    }
}

export default HomeAndSearchRoundedButton;