import { 
    SHOW_BOTTOM_BUTTON,
    SHOW_BOTTOM_MENU,
    PUSH_BOTTOM_MENU_ICON,
    POP_BOTTOM_MENU_ICON,
    RESET_BOTTOM_MENU,
} from '../helpers/ActionNames.js';
import * as defaults from '../helpers/DefaultValues';

//showBottomButton big menu button
//showBottomMenu small bottom menu items
const INITIAL_STATE = {
    showBottomButton: false,
    showBottomMenu: false,
    showCurrentButton: [defaults.buttonNames.menu]
};
  
export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case SHOW_BOTTOM_BUTTON:
            return { ...state, showBottomButton: action.payload };
        case SHOW_BOTTOM_MENU:
            return { ...state, showBottomMenu: action.payload };
        case PUSH_BOTTOM_MENU_ICON:
            return { ...state, showCurrentButton: state.showCurrentButton.concat(action.payload) };
        case POP_BOTTOM_MENU_ICON:
            return { ...state, showCurrentButton:  state.showCurrentButton.slice(0, state.showCurrentButton.length - 1) };
        case RESET_BOTTOM_MENU:
            return { ...state, ...INITIAL_STATE };
        default:
            return state;
    }
};