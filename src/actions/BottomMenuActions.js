import {
    SHOW_BOTTOM_BUTTON,
    SHOW_BOTTOM_MENU,
    PUSH_BOTTOM_MENU_ICON,
    POP_BOTTOM_MENU_ICON,
    RESET_BOTTOM_MENU,
} from '../helpers/ActionNames.js';

export const showBottomMenuAction = (showMenu) => {
    return {
        type: SHOW_BOTTOM_MENU,
        payload: showMenu
    };
}

export const showBottomButtonAction = (showButton) => {
    return {
        type: SHOW_BOTTOM_BUTTON,
        payload: showButton
    };
}

export const pushBottomButtonAction = (buttonName) => {
    return {
        type: PUSH_BOTTOM_MENU_ICON,
        payload: buttonName
    };
}

export const popBottomButtonAction = () => {
    return {
        type: POP_BOTTOM_MENU_ICON,
    };
}

export const resetBottomMenuAction = () => {
    return {
        type: RESET_BOTTOM_MENU,
    };
}