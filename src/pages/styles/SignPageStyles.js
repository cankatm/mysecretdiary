import { 
    StyleSheet, 
    Dimensions,
} from 'react-native';

import * as colors from '../../helpers/ColorPalette';
import * as defaults from '../../helpers/DefaultValues';

//background image size may change!!!
export default StyleSheet.create({
    signPageContainerImageStyle: { 
        width: defaults.WIDTH * 1.2, 
        height: defaults.HEIGHT * 1.2, 
    },
    signPageLogoImageContainerStyle: { 
        width: defaults.signPageLogoSize + 12, 
        height: defaults.signPageLogoSize + 12, 
        borderRadius: defaults.borderRadius,
        backgroundColor: colors.orange,
        alignItems: 'center',
        justifyContent: 'center',
        shadowOffset:{  width: 2,  height: 2 },
        shadowColor: colors.darkBlue,
        shadowOpacity: 0.6,
    },
    signPageLogoImageStyle: { 
        width: defaults.signPageLogoSize - 20, 
        height: defaults.signPageLogoSize - 20, 
    },
    signPageCardContainerStyle: { 
        borderRadius: defaults.borderRadius,
        position: 'absolute', 
        left: defaults.WIDTH / 8, 
        bottom: 120, 
        width: (defaults.WIDTH / 4) * 3, 
        paddingTop: 32,
        paddingBottom: defaults.loginButtonHeight / 2 + 48, 
        backgroundColor: colors.darkBlue,
        shadowOffset:{  width: 2,  height: 2 },
        shadowColor: colors.darkBlue,
        shadowOpacity: 0.6,
        alignItems: 'center',
    },
    signPageTextInputContainerStyle: { 
        width: (defaults.WIDTH / 3) * 2, 
        height: defaults.signPageTextInputContainerSize, 
        backgroundColor: colors.lightGrey, 
        marginVertical: 8,
        borderRadius: defaults.borderRadius,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    signPageTextInputIconContainerStyle: { 
        width: defaults.signPageIconContainerWidth, 
        height: defaults.signPageTextInputContainerSize,
        alignItems: 'center',
        justifyContent: 'center'
    },
    signPageTextInputStyle: { 
        width: ((defaults.WIDTH / 3) * 2) - defaults.signPageIconContainerWidth, 
        height: defaults.signPageTextInputContainerSize, 
        backgroundColor: colors.lightGrey,
        paddingHorizontal: 16,
        color: colors.darkBlue,
        borderTopRightRadius: defaults.borderRadius,
        borderBottomRightRadius: defaults.borderRadius,
    },
    signPageLoginButtonContainerStyle: { 
        position: 'absolute', 
        left: (defaults.WIDTH / 2) - (defaults.loginButtonWidth / 2), 
        bottom: 120 - defaults.loginButtonHeight / 2, 
    },
    signPageLoginButtonInnerContainerStyle: { 
        borderRadius: defaults.borderRadius,
        width: defaults.loginButtonWidth, 
        height: defaults.loginButtonHeight, 
        backgroundColor: colors.orange,
        alignItems: 'center',
        justifyContent: 'center',
        shadowOffset:{  width: 2,  height: 2 },
        shadowColor: colors.darkBlue,
        shadowOpacity: 0.6,
    },
    signPageForgotPasswordAndSignupContainerStyle: { 
        position: 'absolute', 
        bottom: defaults.loginButtonHeight / 2 + 16, 
        left: 16, 
        width: ((defaults.WIDTH / 4) * 3) - 32, 
        height: 15, 
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    signPageForgotPasswordAndSignupButtonContainerStyle: { 
        height: 20, 
        backgroundColor: 'blue', 
        justifyContent: 'center' ,
        backgroundColor: 'transparent'
    },
    signPageForgotPasswordAndSignupButtonTextStyle: { 
        color: colors.lightBlue
    },
    signPageLoginButtonTextStyle: { 
        color: colors.white
    },
});