import { StyleSheet } from 'react-native';

import * as colors from '../../helpers/ColorPalette';
import * as defaults from '../../helpers/DefaultValues';

export default StyleSheet.create({
    homeAndSearchRoundedButtonShadowStyle: {
        shadowColor: colors.orange,
        shadowOpacity: 0.6,
        shadowRadius: 2,
        shadowOffset: {
          height: 1,
          width: 0
        }
    },
    homeAndSearchRoundedButtonContainerStyle: { 
        width: defaults.homePageIconSize, 
        height: defaults.homePageIconSize, 
        borderRadius: defaults.homePageIconSize / 2, 
        backgroundColor: colors.lightGrey,
        marginHorizontal: defaults.marginValue,
        alignItems: 'center',
        justifyContent: 'center'
    },
    homeAndSearchRoundedButtonTextStyle: { 
        fontSize: 16, 
        color: colors.orange 
    },
    squareButtonContainerStyle: {
        alignItems: 'center',
        justifyContent: 'center',
        borderWidth: 1,
    },
    squareButtonTextStyle: {
        color: colors.orange
    },
    keywordButtonContainerStyle: {
        flexDirection: 'row',
        borderWidth: 1,
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    keywordButtonTextContainerStyle: {
        marginLeft: defaults.marginValue
    },
    keywordButtonTextStyle: {
        color: colors.darkestGrey,
        fontSize: 14
    },
    keywordButtonDeleteButtonContainerStyle: {
        width: defaults.deleteButtonSize,
        height: defaults.deleteButtonSize,
        borderRadius: defaults.deleteButtonSize / 2,
        marginRight: defaults.marginValue,
        backgroundColor: colors.red,
        alignItems: 'center',
        justifyContent: 'center'
    },
});