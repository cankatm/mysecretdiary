import { 
    SELECT_FIRST_IMAGE, 
    SELECT_SECOND_IMAGE, 
    SELECT_THIRD_IMAGE, 
    SELECT_FORTH_IMAGE, 
    SELECT_FIFTH_IMAGE, 
    SELECT_SIXTH_IMAGE,
    DELETE_FIRST_IMAGE,
    DELETE_SECOND_IMAGE,
    DELETE_THIRD_IMAGE,
    DELETE_FORTH_IMAGE,
    DELETE_FIFTH_IMAGE,
    DELETE_SIXTH_IMAGE,
    CHANGE_INFORMATION_TEXT,
} from '../helpers/ActionNames';

const INITIAL_STATE = {
    firstImageURL: null,
    secondImageURL: null,
    thirdImageURL: null,
    forthImageURL: null,
    fifthImageURL: null,
    sixthImageURL: null,
    informationText: null
  };
  
  export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
      case SELECT_FIRST_IMAGE:
        return { ...state, firstImageURL: action.payload };
      case SELECT_SECOND_IMAGE:
        return { ...state, secondImageURL: action.payload };
      case SELECT_THIRD_IMAGE:
        return { ...state, thirdImageURL: action.payload };
      case SELECT_FORTH_IMAGE:
        return { ...state, forthImageURL: action.payload };
      case SELECT_FIFTH_IMAGE:
        return { ...state, fifthImageURL: action.payload };
      case SELECT_SIXTH_IMAGE:
        return { ...state, sixthImageURL: action.payload };
      case DELETE_FIRST_IMAGE:
        return { ...state, firstImageURL: null };
      case DELETE_SECOND_IMAGE:
        return { ...state, secondImageURL: null };
      case DELETE_THIRD_IMAGE:
        return { ...state, thirdImageURL: null };
      case DELETE_FORTH_IMAGE:
        return { ...state, forthImageURL: null };
      case DELETE_FIFTH_IMAGE:
        return { ...state, fifthImageURL: null };
      case DELETE_SIXTH_IMAGE:
        return { ...state, sixthImageURL: null };
      case CHANGE_INFORMATION_TEXT:
        return { ...state, informationText: action.payload };
      default:
        return state;
    }
  };