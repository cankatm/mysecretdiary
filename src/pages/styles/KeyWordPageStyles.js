import { 
    StyleSheet, 
    Dimensions,
} from 'react-native';

import * as colors from '../../helpers/ColorPalette';
import * as defaults from '../../helpers/DefaultValues';

export default StyleSheet.create({
    keywordPageStylesContainerStyle: { 
        flex: 1, 
        backgroundColor: colors.white,
        width: defaults.WIDTH, 
        height: defaults.HEIGHT, 
    },
    keywordPageStylesTextInputContainerStyle: { 
        width: defaults.WIDTH - defaults.marginValue * 2,
        height: defaults.buttonHeight,
        backgroundColor: colors.transparent,
        flexDirection: 'row',
    },
    keywordPageStylesTextInputStyle: { 
        width: defaults.WIDTH - defaults.marginValue * 2 - defaults.buttonHeight,
        height: defaults.buttonHeight,
        backgroundColor: colors.darkGrey,
        borderTopLeftRadius: defaults.borderRadius,
        borderBottomLeftRadius: defaults.borderRadius,
        paddingLeft: defaults.marginValue,
        color: colors.darkestGrey,
        borderWidth: 1,
        borderColor: colors.orange
    },
    keywordPageStylesConfirmButtonContainerStyle: { 
        width: defaults.buttonHeight,
        height: defaults.buttonHeight,
        backgroundColor: colors.darkGrey,
        borderTopRightRadius: defaults.borderRadius,
        borderBottomRightRadius: defaults.borderRadius,
        borderWidth: 1,
        borderColor: colors.orange,
        borderLeftWidth: 0,
        alignItems: 'center',
        justifyContent: 'center'
    },
    keywordPageStylesConfirmButtonTextStyle: { 
        color: colors.orange,
        fontSize: 38
    },
    keywordPageStylesTitleStyle: { 
        fontSize: 14,
        color: colors.orange
    },
    keywordPageStylesBottomButtonContainerStyle: { 
        position: 'absolute', 
        bottom: defaults.marginValue, 
        right: defaults.marginValue
    },
    keywordPageStylesBottomButtonInnerContainerStyle: { 
        width: defaults.bottomMenuSize, 
        height: defaults.bottomMenuSize, 
        backgroundColor: colors.green, 
        borderRadius: defaults.bottomMenuSize / 2, 
        alignItems: 'center', 
        justifyContent: 'center' 
    },
});