import React, { Component } from 'react';
import { 
  StyleSheet, 
  Text, 
  View, 
  Dimensions 
} from 'react-native';
import { createStackNavigator } from 'react-navigation';
import Icon from 'react-native-vector-icons/Ionicons';

import * as colors from './ColorPalette';
import * as defaults from './DefaultValues';

import {
    SignPage,
    HomePage,
    SearchPage,
    KeyWordSearchPage,
    WritingEntryPage,
    ReadPage,
    AddPage,
    ShowDayPage,
    KeyWordPage,
    MoodPage,
    SettingsPage,
} from '../pages';

//Animation
const FadeTransition = (index, position) => {
  const sceneRange = [index- 1, index];
  const outputOpacity = [0,1];
  const transition = position.interpolate({
    inputRange: sceneRange,
    outputRange: outputOpacity
  });

  return {
    opacity: transition
  }
}

//Animation navigation config
const NavigationConfig = () => {
  return {
    screenInterpolator: (sceneProps) => {
      const position = sceneProps.position;
      const scene = sceneProps.scene;
      const index = scene.index;

      return FadeTransition(index, position);
    }
  }
}

export const MainNavigator = createStackNavigator({
    HomePage: { screen: HomePage }, 
    ReadPage: { screen: ReadPage },
    SignPage: { screen: SignPage },
    WritingEntryPage: { screen: WritingEntryPage },
    SearchPage: { screen: SearchPage },
    KeyWordSearchPage: { screen: KeyWordSearchPage },
    AddPage: { screen: AddPage },
    ShowDayPage: { screen: ShowDayPage },
    KeyWordPage: { screen: KeyWordPage },
    MoodPage: { screen: MoodPage },
    SettingsPage: { screen: SettingsPage },
  },
  {
    transitionConfig: NavigationConfig,
    headerMode: 'none',
    lazyLoad: true,
    navigationOptions: {
      gesturesEnabled: false
    }
  }
);