import { StyleSheet } from 'react-native';

import * as colors from '../../helpers/ColorPalette';
import * as defaults from '../../helpers/DefaultValues';

export default StyleSheet.create({
    bottomMenuContainerStyle: {
        position: 'absolute', 
        bottom: 16, 
        right: 16
    },
    bottomMenuInnerContainerStyle: { 
        width: defaults.bottomMenuSize, 
        height: defaults.bottomMenuSize, 
        borderRadius: defaults.bottomMenuSize / 2, 
        backgroundColor: colors.orange,
    },
    bottomMenuItemContainerStyle: { 
        position: 'absolute',
    },
    bottomMenuItemInnerContainerStyle: { 
        width: defaults.bottomMenuSmallIconsSize, 
        height: defaults.bottomMenuSmallIconsSize, 
        borderRadius: defaults.bottomMenuSmallIconsSize / 2, 
        backgroundColor: colors.orange,
        alignItems: 'center',
        justifyContent: 'center'
    },
});