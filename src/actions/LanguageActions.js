import {
    SELECT_LANGUAGE
} from '../helpers/ActionNames.js';

export const selectLanguageAction = (language) => {
    return {
        type: SELECT_LANGUAGE,
        payload: language
    };
}