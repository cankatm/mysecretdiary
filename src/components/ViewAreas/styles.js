import { StyleSheet } from 'react-native';

import * as colors from '../../helpers/ColorPalette';
import * as defaults from '../../helpers/DefaultValues';

export default StyleSheet.create({
    moodViewAreaContainerStyle: {
        borderWidth: StyleSheet.hairlineWidth,
        borderColor: colors.orange,
        marginHorizontal: defaults.marginValue / 4,
        marginVertical: defaults.marginValue / 2,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: defaults.borderRadius
    },
    moodViewAreaTextStyle: {
        padding: defaults.marginValue / 2,
        color: colors.darkestGrey,
        fontSize: 14
    }
});